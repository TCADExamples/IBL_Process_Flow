
String NDOSE = "@NDOSE@";
String NENERGY = @NENERGY@;
String PSPRAYENERGY = @PSPRAYENERGY@;

defop Oxide_processing ()
{
  deposit (material : "Oxide", thickness : 100 nm, dopant : "default");
  pattern (layer : "NIMPLANT", polarity : dark_field);
  etch (material : "Oxide", thickness : 50 nm, etch_type : anisotropic, side : both);
  etch (material : "Resist", etch_type : strip, etchstop : "default");
  implant (species : "phosphorus", dose : $NDOSE /cm2, energy : "${NENERGY}" keV, tilt : 0 deg);
  !!insert (dios : "", sprocess : "pdbSet Grid Adaptive 1
pdbSet Grid SnMesh DelaunayType boxmethod

refinebox name= Global refine.min.edge= {0.1 0.1 0.1} refine.max.edge= {1000 1000 1000} refine.fields= { NetActive Boron Phosphorus} def.max.asinhdiff= 2.5 adaptive all add
grid remesh", sde : "", tsuprem4 : "");
  save (basename : "after_oxide_etch_n@node@", format : plot, dios : "", sprocess : "", sde : "", tsuprem4 : "");
}

defop nitride_processing ()
{
  deposit (material : "Nitride", thickness : 200 nm, side : both, deposition_type : anisotropic);
  pattern (layer : "NITRIDEOPENING", polarity : dark_field);
  etch (material : "Nitride", thickness : 200 nm, etch_type : anisotropic, etchstop : "default");
  etch (material : "Resist", etch_type : strip, etchstop : "default", side : both);
  implant (species : "boron", dose : 1e12 /cm2, energy : "${PSPRAYENERGY}" keV, tilt : 0 deg);
  !!insert (dios : "", sprocess : "pdbSet Grid Adaptive 1
pdbSet Grid SnMesh DelaunayType boxmethod

refinebox name= Global refine.min.edge= {0.1 0.1 0.1} refine.max.edge= {1000 1000 1000} refine.fields= { NetActive Boron Phosphorus} def.max.asinhdiff= 2.5 adaptive all add
grid remesh", sde : "", tsuprem4 : "");
  save (basename : "after_nitride_etch_n@node@", format : plot, dios : "", sprocess : "", sde : "", tsuprem4 : "");
}

defop Oxide_holes ()
{
  pattern (layer : "OXIDEOPENING", polarity : dark_field);
  etch (material : "Oxide", etch_type : anisotropic, side : both);
  etch (material : "Resist", etch_type : strip);
  save (basename : "after_oxide_hole_etch_n@node@", format : plot, dios : "", sprocess : "", sde : "", tsuprem4 : "");
}

defop aluminum_deposition ()
{
  deposit (material : "Aluminum", thickness : 1500 nm);
  pattern (layer : "CANOD", polarity : light_field);
  etch (material : "Aluminum", etch_type : anisotropic);
  etch (material : "Resist", etch_type : strip);
  save (basename : "top_n@node@", format : plot, dios : "", sprocess : "", sde : "", tsuprem4 : "");
}

defop Backside_oxide_processing ()
{
  deposit (material : "Oxide", thickness : 100 nm, dopant : "default");
  pattern (layer : "BACKPIMP", polarity : dark_field);
  etch (material : "Oxide", thickness : 50 nm, etch_type : anisotropic, etchstop : "default");
  etch (material : "Resist", etch_type : strip, etchstop : "default");
  implant (species : "boron", dose : 1e15 /cm2, energy : 60 keV, tilt : 0 deg);
  !!insert (sprocess : "pdbSet Grid Adaptive 1
pdbSet Grid SnMesh DelaunayType boxmethod

refinebox name= Global refine.min.edge= {0.1 0.1 0.1} refine.max.edge= {1000 1000 1000} refine.fields= { NetActive Boron Phosphorus} def.max.asinhdiff= 2.5 adaptive all add
grid remesh");
  save (basename : "back_after_oxide_etch_n@node@", format : plot, dios : "", sprocess : "", sde : "", tsuprem4 : "");
}

defop backside_nitride_processing ()
{
  deposit (material : "Nitride", thickness : 200 nm, dopant : "default", deposition_type : anisotropic);
  pattern (layer : "BACKNITRIDEOPENING", polarity : dark_field);
  etch (material : "Nitride", thickness : 200 nm, etch_type : anisotropic, etchstop : "default");
  etch (material : "Resist", etch_type : strip, etchstop : "default");
  anneal (time : 240 min, temperature : 950 degC, nitrogen : 100 %, side : front);
  !!insert (sprocess : "pdbSet Grid Adaptive 1
pdbSet Grid SnMesh DelaunayType boxmethod

refinebox name= Global refine.min.edge= {0.1 0.1 0.1} refine.max.edge= {1000 1000 1000} refine.fields= { NetActive Boron Phosphorus} def.max.asinhdiff= 2.5 adaptive all add
grid remesh");
  save (basename : "back_after_nitride_etch_n@node@", format : plot, dios : "", sprocess : "", sde : "", tsuprem4 : "");
}

defop Backside_oxide_holes ()
{
  pattern (layer : "BACKOXOPENING", polarity : dark_field);
  etch (material : "Oxide", etch_type : anisotropic, etchstop : "default");
  etch (material : "Resist", etch_type : strip, etchstop : "default");
  save (basename : "back_after_oxide_hole_etch_n@node@", format : plot, dios : "", sprocess : "", sde : "", tsuprem4 : "");
}

defop backside_Aluminum_deposition ()
{
  deposit (material : "Aluminum", thickness : 1500 nm, dopant : "default");
  pattern (layer : "CBASE", polarity : light_field);
  etch (material : "Aluminum", etch_type : anisotropic, etchstop : "default");
  etch (material : "Resist", etch_type : strip, etchstop : "default");
  save (basename : "final_n@node@", format : plot, dios : "", sprocess : "", sde : "", tsuprem4 : "");
}

#header
environment (title : "ligament test", save : true, grid : true, debug : false, check1d : false, analytical : false, simulator : sprocess, mask : Struct {
}, region : @REGION@, coordinate_shift : true, output : "n@node@", node : "@node@", side : front, graphics : false, depth : 250 um, user_grid : Struct {
  sprocess : "line y loc= 50 spa=151.34 tag=left
line y loc=60 spa=20
line y loc=75 spa=40
line y loc=90 spa=20
line y loc=100 spa=151.34 tag=right


line x loc=0 spa=0.05 tag=top
line x loc=0.3 spa=0.1 
line x loc=2 spa=1 
#line x loc=125 spa=50
#line x loc=248 spa=2 
#line x loc=249.7 spa=0.2
line x loc=250 spa=25 tag=bottom

line z loc=-756.1 spa=30.0 tag=front
line z loc=-500 spa=10.0 
line z loc=-480 spa=30.0
 line z loc=-60 spa=30.0
line z loc=-50 spa=10.0 
line z loc=-40 spa=200
line z loc=-30 spa=10.0 
line z loc=0.6 spa=30.0 tag=back

region Silicon xlo=top xhi=bottom ylo=left yhi=right zlo=front zhi=back";
}, grid_refinement : Struct {
  dios : "repl(cont(maxtrl=5,RefineGradient=-6,RefineMaximum=0,RefineJunction=-6,RefineBoundary=-6))";
  sprocess : "default";
}, tsuprem4_delta_vertical : 0.5 um, tsuprem4_delta_horizontal : 0.5 um, tsuprem4_min_vertical : 0.1 um, tsuprem4_min_horizontal : 0.1 um);
#endheader
substrate (dopant : "phosphorus", resistivity : 5000 ohm-cm);
insert (sprocess : "
math numThreads=8
");
insert (sprocess : "
grid remesh
");
insert (sprocess : "
pdbSet Grid Adaptive 1
pdbSet Grid AdaptiveField Refine.Abs.Error 1e25
pdbSet Grid AdaptiveField Refine.Rel.Error 2.0
pdbSet Grid Phosphorus Refine.Min.Value 1e25
pdbSet Grid Phosphorus Refine.Max.Value 1e25
pdbSet Grid Phosphorus Refine.Target.Length 10
pdbSet Grid Boron Refine.Min.Value 1e25
pdbSet Grid Boron Refine.Max.Value 1e25
pdbSet Grid Boron Refine.Target.Length 100

refinebox name=Pixel refine.fields= {Boron Phosphorus} rel.error= {Boron=10 Phosphorus=10}  abs.error= {Boron=@BoronError@ Phosphorus=@PhosphorusError@} Adaptive min= {0 55 -500 } max= {1.2 95 0} refine.min.edge= {0.05 0.5 0.5 } all add
");
save (basename : "initial_n@node@", format : plot);
#split @Oxide_processing@
Oxide_processing ();
#split @nitride_processing@
nitride_processing ();
!!load (basename : "after_nitride_etch", dios : "", sprocess : "", sde : "", tsuprem4 : "");
#split @Oxide_holes@
Oxide_holes ();
#split @aluminum_deposition@
aluminum_deposition ();
insert (dios : "

", sprocess : "
transform flip
", sde : "

", tsuprem4 : "

");
insert (dios : "

", sprocess : "
pdbSet Grid Adaptive 1
pdbSet Grid AdaptiveField Refine.Abs.Error 1e25
pdbSet Grid AdaptiveField Refine.Rel.Error 2.0
pdbSet Grid Phosphorus Refine.Min.Value 1e25
pdbSet Grid Phosphorus Refine.Max.Value 1e25
pdbSet Grid Phosphorus Refine.Target.Length 10
pdbSet Grid Boron Refine.Min.Value 1e25
pdbSet Grid Boron Refine.Max.Value 1e25
pdbSet Grid Boron Refine.Target.Length 100

refinebox name=bottombox refine.fields= {Boron} rel.error= {Boron=0.6}  abs.error= {Boron=@BoronError@} Adaptive min= {248.8 55 -500 } max= {250 95 0} refine.min.edge= {0.05 0.5 0.5 } all add
", sde : "

", tsuprem4 : "

");
#split @Backside_oxide_processing@
Backside_oxide_processing ();
#split @backside_nitride_processing@
backside_nitride_processing ();
#split @Backside_oxide_holes@
Backside_oxide_holes ();
#split @backside_Aluminum_deposition@
backside_Aluminum_deposition ();


